<?php
include(dirname(__FILE__) . '/database.php');
$msg = showMsg();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap-5.1.1-dist/css/bootstrap.min.css">
    <script src="bootstrap-5.1.1-dist/js/bootstrap.bundle.js"></script>
    <link rel="stylesheet" href="style.css">
    <title>留言板</title>
</head>


<body>
    <header class="navbar d-flex">
        <div class="logo">留言板</div>
    </header>
    <div class="container">
        <div class="list">
            <h1 class="list_title text-center mt-5 mb-3">留言列表</h1>
            <?php foreach ($msg as $key => $row) { ?>
                <div class="card mb-2">
                    <div class="card-body">
                        <h5 class="card-title">
                            <?php echo $row['name'] ?>
                        </h5>
                        <p class="card-text">
                            <?php echo $row['content'] ?>
                        </p>
                        <form id="msg<?php echo $row['msg_id'] ?>" class="d-inline" method="post">
                            <input type="hidden" name="msg_id" value="<?php echo $row['msg_id'] ?>">
                            <a type="button" class="btn btn-primary" data-bs-toggle="modal"
                                data-bs-target="#staticBackdrop<?php echo $row['msg_id'] ?>">修改</a>
                            <input type="submit" name="delect_msg" class="btn btn-danger" value="刪除"></input>
                        </form>
                        <span class="date_text">
                            <?php echo $row['date'] ?>
                        </span>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="add_line text-center mt-5">
            <a class="btn btn-success px-5 mb-3" id="add_expend" data-bs-toggle="collapse" href="#collapseExample"
                role="button" aria-expanded="false" aria-controls="collapseExample" onclick="disappearance()">新增留言</a>
            <div class="collapse" id="collapseExample">
                <div class="card card-body">
                    <form method="post">
                        <input class="d-block form-control mb-2" type="text" name="user_name" placeholder="留言者姓名"
                            required>
                        <textarea class="d-block form-control" name="new_msg" rows="10" placeholder="請輸入內容"
                            required></textarea>
                        <input class="btn btn-success px-5 mt-3" type="submit" value="確認新增" name="add_msg">
                    </form>
                </div>
            </div>
        </div>

        <!-- 以下為修改的彈出視窗 -->
        <?php foreach ($msg as $key => $row) { ?>
            <div class="modal fade" id="staticBackdrop<?php echo $row['msg_id'] ?>" data-bs-backdrop="static"
                data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">修改留言</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <textarea class="w-100 form-control" type="text" placeholder="請輸入修改內容"
                                form="msg<?php echo $row['msg_id'] ?>" name="edited_content" required><?php echo $row['content'] ?></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" form="msg<?php echo $row['msg_id'] ?>" name="edit_msg">確認修改</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

    <script>
        function disappearance() {
            document.querySelector('#add_expend').style.display = "none";
        }
    </script>
</body>

</html>
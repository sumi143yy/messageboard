<?php
$PDO = new PDO('mysql:host=localhost;port=3306;dbname=message_board', 'root', '');
$PDO->query('set names utf8');

function addMsg($userName, $newMsg)
{
    $dateTime = new DateTime();
    $today = $dateTime->format('Y-m-d'); // 抓取現在時間 Y-m-d 表示抓取 (年-月-日)(20XX-XX-XX)
    $sql = "INSERT INTO `message` (`name`, `content`, `date`) VALUES (?,?,?)";
    global $PDO;
    $stmt = $PDO->prepare($sql);
    $stmt->execute([$userName, $newMsg, $today]);
}

function showMsg()
{
    $sql = "SELECT * FROM `message`";
    global $PDO;
    $stmt = $PDO->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchAll();
    return $result;
}

function delectMsg($msgId)
{
    $sql = "DELETE FROM `message` WHERE `message`.`msg_id` = ?";
    global $PDO;
    $stmt = $PDO->prepare($sql);
    $stmt->execute([$msgId]);
}

function editMsg($msgId, $edited_content)
{
    $dateTime = new DateTime();
    $today = $dateTime->format('Y-m-d');
    $sql = "UPDATE `message` SET `content`= ?,`date`= ? WHERE `message`.`msg_id` = ?";
    global $PDO;
    $stmt = $PDO->prepare($sql);
    $stmt->execute([$edited_content, $today, $msgId]);
}

if (isset($_POST['add_msg'])) {
    addMsg($_POST['user_name'], $_POST['new_msg']);
}

if (isset($_POST['delect_msg'])) {
    delectMsg($_POST['msg_id']);
}

if (isset($_POST['edit_msg'])) {
    editMsg($_POST['msg_id'], $_POST['edited_content']);
}